function displayMsgToSelf(){
	console.log("You're doing great today!")
};

// displayMsgToSelf();
// displayMsgToSelf();
// displayMsgToSelf();
// displayMsgToSelf();
// displayMsgToSelf();
// displayMsgToSelf();
// displayMsgToSelf();
// displayMsgToSelf();
// displayMsgToSelf();
// displayMsgToSelf();

let count = 10;

while (count !== 0){
	displayMsgToSelf();
	count--;
};
//While Loop
	//will allow us to repeat an instruction as long as the condition is true.
/*
	Syntax:

*/

let number = 5;
while (number !== 0){
	console.log("While" + " " + number);
	number--;
};

//count from 5-10
while (number <= 10){
	console.log("While" + " " + number);
	number++;
};

//Do-While Loop
	// loop works a lot like while
	// do-while guarantees to run at least once
	/*
		Syntax:

	*/
// Number function works like parseInt, it change a string type to number data type.	
let number2 = Number(prompt("Give me a number")
	)
do{
	// the current value of the number2 is printed out
	console.log(`Do While ${number2}`);

	//increases the number by 1 after every iteration of the loop
	number2 +=1;

	//while(condition)
} while (number2 < 10 );

let counter = 1

do {
	console.log(counter);
	counter++;
} while (counter <=21)

// For Loop
	//A more flexible loop than while and do-while loop.
	// It consists of three parts:
	// initialization, condition and finalExpression
		//Initialization - determines where the loop starts
		//Condition  - determines when the loop stops.
		// finalExpression - indicates how to the advance the loop(incrementation or decrementation)
	/*
		Syntax:
		for (initialization; condition; finalExpresion){
			
		}
	*/

for (let count = 0; count <= 20; count++){
	console.log(count)
};

let myString = "alex"
console.log(myString.length);
	//result: 4
// strings are special compared to other data types.
	//it has access to functions and other pieces of information another primitive might not have
	console.log(myString[0]);
		//result: a

	for (let x = 0; x < myString.length; x++){
		console.log(myString[x])
	};

	let myName = "Kimberly"

	for (let i = 0; i < myName.length; i++){
		if (
			myName[i].toLowerCase() == "a" ||
			myName[i].toLowerCase() == "e" ||
			myName[i].toLowerCase() == "i" ||
			myName[i].toLowerCase() == "o" ||
			myName[i].toLowerCase() == "u" 
			){
			console.log(3)
		} else {
			console.log(myName[i])
		}
	};
// Continue and Break Statements
	for (let count = 0; count <=20; count++){
		if(count % 2 == 0){
			continue;
		}
		console.log(`Continue and Break ${count}`)
		if(count > 10 ){
			break;
		}
	};

	let name = "alexandro";

	for( let i = 0; i < name.length; i++){
		console.log(name[i]);
		if(name[i].toLowerCase() == "a"){
			console.log("Continue to the next iteration")
			continue;
		}
		if( name[i] == "d"){
			break;
		}
	};




